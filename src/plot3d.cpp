#include "plot3d.h"

#ifdef ALLOW_GRAPHICS
#include "proteins.h"
#include "lattice.h"
#include "zaptrackball.h"
#include "zaprandom.h"
// the openGL.dll can be found in C:/windows/SysWOW64
//file:///C:/Windows/SysWOW64/opengl32.dll
// the library can be found in:
// C:\Qt\Qt5.7.0\Tools\mingw530_32\i686-w64-mingw32\lib
// the headers are in :
// C:\Qt\Qt5.7.0\Tools\mingw530_32\i686-w64-mingw32\include
#include <sstream>
/*#ifdef WINDOWS
#include "freeglut/include/GL/glut.h"
#else
#include <GL/glut.h>
#endif*/

void display();
void init();

/* #include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
*/

// beware of capital letters
#include "soil/SOIL.h"

#include <iostream>
#include <iomanip> // setprecision
using namespace std;


vector<protein*> currentProts;
vector<set<int>*> currentSurfaces;
vector<vector<double>> currentPoints;

//int spinning = 0,
bool moving = 0;
int beginx, beginy;     // saves beginning position when clicked move
int W = 300, H = 300; // resizing needs to be done ...

float curquat[4];   // current quaternion
float lastquat[4];

GLdouble bodyWidth = 3.0;
bool newModel = 1;
int scaling = 1;
float scalefactor = 1.0;
bool waitRightClick = false;
int indexStructDisplayed = -1;
vector<bool> keepAlways;

void print3DState(){
    cout << /*"Spinning " << spinning <<*/ ", moving " << moving << ", beginx " << beginx << ", beginy " << beginy << ", bodyWidth " << bodyWidth << ", scaleFactor " << scalefactor << endl;
}

void waitForRightClick(){
    waitRightClick = true;
    while(waitRightClick){
        glutMainLoop();
    }
}

void recalcModelView(void)
{
  GLfloat m[4][4];
  glPopMatrix(); //???
  glPushMatrix();
  build_rotmatrix(m, curquat);
  glMultMatrixf(&m[0][0]);
  if (scalefactor == 1.0) {
    glDisable(GL_NORMALIZE);
  } else {
    glEnable(GL_NORMALIZE);
  }
  //cout << "Called" << endl;
  glScalef(scalefactor, scalefactor, scalefactor);
  //glTranslatef(-8, -8, -bodyWidth / 2);
  newModel = 0;
}

/*
void showMessage(GLfloat x, GLfloat y, GLfloat z, char *message)
{
  glPushMatrix();
  glDisable(GL_LIGHTING);
  glTranslatef(x, y, z);
  glScalef(.02, .02, .02);
  while (*message) {
    glutStrokeCharacter(GLUT_STROKE_ROMAN, *message);
    message++;
  }
  glEnable(GL_LIGHTING);
  glPopMatrix();
}

done inside display
void redraw(void)
{
  if (newModel)
    recalcModelView();
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //glCallList(DINOSAUR);
  showMessage(2, 7.1, 4.1, "Spin me.");
  glutSwapBuffers();
}*/

void myReshape(int w, int h)
{
  glViewport(0, 0, w, h);
  W = w;
  H = h;
}

void mouse(int button, int state, int x, int y)
{
  if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
    //spinning = 0;
    glutIdleFunc(NULL);
    moving = 1;
    beginx = x;
    beginy = y;
    // glutPostRedisplay will be catched later because of motion function !!
    /*
    if (glutGetModifiers() & GLUT_ACTIVE_SHIFT) {
      scaling = 1;
    } else {
      scaling = 0;
    }*/

  }
  if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
    moving = 0;
  }
  if(button == 3){
      //cout << "scroll up" << endl; // note: always called twice
      scalefactor *= 1.08;
      newModel = 1;             // needs to recompute the model (object)
      glutPostRedisplay(); // needs
  }
  if(button == 4){
      //cout << "scroll down" << endl;
      scalefactor *= 0.92;
      newModel = 1;
      glutPostRedisplay();
  }
}

/*
void animate(void)
{
  add_quats(lastquat, curquat, curquat);
  newModel = 1;
  glutPostRedisplay();
}*/

void motion(int x, int y)
{
  /*if (scaling) {
    //scalefactor = scalefactor * (1.0 + (((float) (beginy - y)) / H));
    beginx = x;
    beginy = y;
    newModel = 1;
    glutPostRedisplay();
    return;
  }*/
  if (moving) {
    trackball(lastquat,
      (2.0 * beginx - W) / W,
      (H - 2.0 * beginy) / H,
      (2.0 * x - W) / W,
      (H - 2.0 * y) / H
      );
    beginx = x;
    beginy = y;
    //spinning = 1;

    /////glutIdleFunc(animate);
    ///
    /// instead, does it only once
    add_quats(lastquat, curquat, curquat);
    newModel = 1;
    glutPostRedisplay();
  }
}

GLboolean lightZeroSwitch = GL_TRUE, lightOneSwitch = GL_TRUE;

void
controlLights(int value)
{
  switch (value) {
  case 1:
    lightZeroSwitch = !lightZeroSwitch;
    if (lightZeroSwitch) {
      glEnable(GL_LIGHT0);
    } else {
      glDisable(GL_LIGHT0);
    }
    break;
  case 2:
    lightOneSwitch = !lightOneSwitch;
    if (lightOneSwitch) {
      glEnable(GL_LIGHT1);
    } else {
      glDisable(GL_LIGHT1);
    }
    break;
#ifdef GL_MULTISAMPLE_SGIS
  case 3:
    if (glIsEnabled(GL_MULTISAMPLE_SGIS)) {
      glDisable(GL_MULTISAMPLE_SGIS);
    } else {
      glEnable(GL_MULTISAMPLE_SGIS);
    }
    break;
#endif
  case 4:
    glutFullScreen();
    break;
  case 5:
    exit(0);
    break;
  }
  glutPostRedisplay();
}

void
vis(int visible)
{
  if (visible == GLUT_VISIBLE) {
    //if (spinning)
    //  glutIdleFunc(animate);
  } else {
    //if (spinning)
    //  glutIdleFunc(NULL);
  }
}














void drawBitmapText(string s,float x,float y,float z)
{
     glRasterPos3f(x, y,z);
    for(unsigned int i = 0; i < s.size(); ++i)
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int) s[i]);
}

void drawBitmapText2D(string s,float x,float y)
{
     glRasterPos2f(x, y);
    for(unsigned int i = 0; i < s.size(); ++i)
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int) s[i]);
}

void keyboard(unsigned char key, int /*x*/, int /*y*/){
    switch(key) {
        case 'q':
        indexStructDisplayed--;
        if(indexStructDisplayed < -1) indexStructDisplayed = keepAlways.size() - 1;
        break;
        case 'd':
        indexStructDisplayed++;
        //cout << keepAlways.size() << endl;
        if(indexStructDisplayed >= (int) keepAlways.size()) indexStructDisplayed = -1;
        break;
        default:
        break;
    }
    glutPostRedisplay(); /* this redraws the scene without
        waiting for the display callback so that any changes appear
        instantly */
}

void glDisplay(int argc, char** argv)
{
    if(!argv){
        char* a = (char*) "a";
        char* v[] = {a};
        argv = v;
        //char *c[] = {(char*)"Hello",NULL};
    }
    //Initialize windows
    glutInit(&argc, argv);
    //Single = one buffer ; double = work on a hidden buffer and then swap
    //glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB ); /// now with 2 buffers : one for working, one for showing, and swap each time
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH | GLUT_MULTISAMPLE);

    glutInitWindowSize(500, 500);
    glutInitWindowPosition(200,200);

    trackball(curquat, 0.0, 0.0, 0.0, 0.0);

    glutCreateWindow("Trial");

    glutDisplayFunc(display);
    glutReshapeFunc(myReshape);
//    glutVisibilityFunc(vis);
    glutMouseFunc(mouse);
    // glutMouseWheelFunction(mouse); doesn't exist on the official glut => get button 3 and 4
    glutMotionFunc(motion);

    glutKeyboardFunc(keyboard);
    /*    glutCreateMenu(controlLights);
    glutAddMenuEntry("Toggle right light", 1);
    glutAddMenuEntry("Toggle left light", 2);
    glutAddMenuEntry("Full screen", 3);
    glutAddMenuEntry("Quit", 4);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    glEnable(GL_CULL_FACE);

*/
//    makeDinosaur();

    // in case you want to use Devil library
    /*ilInit();
    iluInit();
    ilutRenderer(ILUT_OPENGL);*/

    init();

    // the display function is called automatically if sth needs to be redisplayed.
    //glutDisplayFunc(display);//(somatic, CTL));
    //glutMainLoop();
}


void init(void)
{
    //Background. Last 0.0 is for transparency
    glClearColor(0.0, 0.0, 0.0, 1.0);
    //glClearColor(1.0, 1.0, 1.0, 1.0);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    // to specify another position for the light
    GLfloat lightOnePosition[] = {0, 0, ZWidth, 0.0};
    glLightfv(GL_LIGHT0, GL_POSITION, lightOnePosition);
    GLfloat lightOneColor[] = {1.0, 1.0, 1.0, 1.0};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lightOneColor);    // very important
    // OR/AND
    //GLfloat specular[] = {1.0f, 1.0f, 1.0f , 1.0f};
    //glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
    // OR/AND
    //GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
    //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);

#define zoom 1
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, 1.0, 1, 500); // Note : deph test works only if the first plane is > 0
    glMatrixMode(GL_MODELVIEW);
    gluLookAt(0, 0, ZWidth * 2.0 /*+ ZWidth*/,  /* eye is at (0,0,30) */
      0, 0, 0,      /* center is at (0,0,0) */
      0.0, 1.0, 0.);      /* up is in positive Y direction */
    //glOrtho(-0.1, 0.1, -0.1, 0.1, -0.1, 0.1);
    //glPushMatrix(); /// Phi: might need to remove
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_DIFFUSE);
    glEnable(GL_DEPTH_TEST);
}



void addToDisplay(protein* new_s, bool always_visible){
    if(!new_s) {cerr << "ERR: addToDisplay(NULL)" << endl; return;}
    keepAlways.push_back(always_visible);


    //for(unsigned int i = 0; i < currentProts.size(); ++i){
    //    if(currentProts[i] == new_s) return;
    //}
    currentProts.push_back(new_s);
}

void addToDisplay(struct3D* new_s, bool always_visible){
    protein* protForDisplay = new protein(*new_s);
    addToDisplay(protForDisplay, always_visible);
}

void addDissected(protein* new_s, moveDirection startDir, int startPos){
    // Note: done from central position and undefined start.
    if(!new_s) {cerr << "ERR: addDissected(NULL)" << endl; return;}
    string S = new_s->sequence;
    int L = S.size();
    // Less than 2 moves doesn't need dissect
    if(L <= 2){
        addToDisplay(new_s, false);
    }
    for(int i = 0; i < L; ++i){
        protein* s2 = new protein(new_s->sequence.substr(0,i+1), startPos);
        addToDisplay(s2, false);
    }
}



void addToDisplay(set<int>* new_s){
    for(unsigned int i = 0; i < currentSurfaces.size(); ++i){
        if(currentSurfaces[i] == new_s) return;
    }
    currentSurfaces.push_back(new_s);
}

void addToDisplay(vector<vector<double>>* listpoints){
    if(!listpoints) return;
    currentPoints.clear();
    for(unsigned int i = 0; i < listpoints->size(); ++i){
        currentPoints.push_back((*listpoints)[i]);
    }
}

void clearDisplay(){
    currentProts.clear();
    currentSurfaces.clear();
    currentPoints.clear();
}


void display(){
    //cout << "Display - " << indexStructDisplayed << endl;
    if (newModel)
      recalcModelView();

    // Question : how to write text on the screen ???
    /*glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glColor3f(1.0f, 0.0f, 0.0f);//needs to be called before RasterPos
    text ???
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();*/

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    //when drawing something, give color first, then draw
    //f signifies float
    glLineWidth(3.0);
    glColor4f(0.45, 0.0, 0.45, 1.0);
    glColor4f(1, 1, 1, 1.0);

    // Borders of the cube
    for(int ix = 0; ix <=1; ++ix){
        for(int iy = 0; iy <=1; ++iy){
            for(int iz = 0; iz <=1; ++iz){
                glBegin(GL_LINES);
                glVertex3f(XWidth * (ix-0.5) , YWidth * (iy-0.5) , -ZWidth * (iz-0.5) );
                glVertex3f(XWidth * (ix-0.5) , YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glVertex3f(XWidth * (ix-0.5) , YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glVertex3f(XWidth * (ix-0.5) , - YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glVertex3f(XWidth * (ix-0.5) , - YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glVertex3f(XWidth * (ix-0.5) , YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glVertex3f(XWidth * (ix-0.5) , YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glVertex3f(- XWidth * (ix-0.5) , YWidth * (iy-0.5) , ZWidth * (iz-0.5) );
                glEnd();
            }
        }
    }

    // Axes
    glColor4f(0.3, 0.3, 0.85, 1.0);
    for(int i = 0; i < XWidth ; ++i){
        glBegin(GL_LINES);
        glVertex3f(i -0.1 - 0.5*XWidth, 0 , 0 );
        glVertex3f(i + 0.1 - 0.5*XWidth, 0 , 0 );
        glEnd();
        glBegin(GL_LINES);
        glVertex3f(0,i - 0.1 - 0.5*YWidth, 0  );
        glVertex3f(0,i + 0.1 - 0.5*YWidth, 0 );
        glEnd();
        glBegin(GL_LINES);
        glVertex3f(0,0,i - 0.1  - 0.5*ZWidth);
        glVertex3f(0,0,i + 0.1 - 0.5*ZWidth);
        glEnd();
    }
    drawBitmapText("X",XWidth / 2.0,0,0);
    drawBitmapText("Y",0,YWidth / 2.0,0);
    drawBitmapText("Z",0,0,ZWidth/2.0);

    #define randomCoeff 0
    #define sizeSphere 0.5
    for(unsigned int is = 0; is < currentProts.size(); ++is){
        if((indexStructDisplayed == -1) || (indexStructDisplayed == (int) is) || (keepAlways[is])){
            protein* s = currentProts[is];
            if(s == NULL) continue;
            stringstream tt;
            tt << "S="<< s->sequence;
            drawBitmapText(tt.str().c_str(),-XWidth / 3.0, -YWidth/2.0 -is*5 , 0);

            int nR = s->points.size();
            vector<int> posPrec;
            vector<int> pos;

            if(keepAlways[is]){
                    glColor4f(1.0, 1.0, 1.0, 1.0);
            } else {
                double dr = random::uniformDouble(0,1);
                double dg = random::uniformDouble(0,1);
                double db = random::uniformDouble(0,1);
                glColor4f(dr, dg, db, 0.2);
            }
            double dx1 = random::uniformDouble(0,randomCoeff*0.2);
            double dy1 = random::uniformDouble(0,randomCoeff*0.2);
            double dz1 = random::uniformDouble(0,randomCoeff*0.2);
            for(int i = 0; i < nR; ++i){
                posPrec = pos;
                pos = lattice::positionFromID(s->points[i].IDposition);
                //GLfloat d1[] = { 0.1, 0.4, 0.9, 1.0 };
                //glMaterialfv(GL_FRONT,GL_DIFFUSE,d1);
                //glEnable(GL_COLOR_MATERIAL);
                //glColor3f(0.1, 0.4, 0.9);




//                    if( i < 2) // first 2 positions are different
//                        glColor4f(0.8, 1.0, 0.8, 1.0);
//                    else
//                        glColor4f(1.0, 0.3, 0.3, 1.0);

                glTranslatef(dx1 + pos[0] - XWidth / 2., dy1 + pos[1] -YWidth / 2., dz1 + pos[2] - ZWidth / 2.);
                glutSolidSphere(sizeSphere, 50, 50);
                glTranslatef(-dx1 -pos[0] + XWidth / 2., -dy1 -pos[1] +YWidth / 2., -dz1 -pos[2] + ZWidth / 2.);
                if(i > 0){
                    glBegin(GL_LINES);
                    glVertex3f(dx1 + posPrec[0] - XWidth / 2.,dy1 + posPrec[1] -YWidth / 2., dz1 + posPrec[2] - ZWidth / 2.);
                    glVertex3f(dx1 + pos[0] - XWidth / 2., dy1 + pos[1] -YWidth / 2., dz1 +pos[2] - ZWidth / 2.);
                    glEnd();
                }
            }
        }
    }
    #define sizeSphereSurface 0.12
    for(unsigned int is = 0; is < currentSurfaces.size(); ++is){
        set<int>* srf = currentSurfaces[is];
        if(srf == NULL) continue;

        //stringstream tt;
        //tt << "S="<< s->sequence;
        //drawBitmapText(tt.str().c_str(),-XWidth / 3.0, -YWidth/2.0 -is*5 , 0);

        std::set<int>::iterator it;
        for(it = srf->begin(); it != srf->end(); ++it){
            vector<int> pos = lattice::positionFromID(*it);
            //GLfloat d1[] = { 0.1, 0.4, 0.9, 1.0 };
            //glMaterialfv(GL_FRONT,GL_DIFFUSE,d1);
            //glEnable(GL_COLOR_MATERIAL);
            //glColor3f(0.1, 0.4, 0.9);
            glColor4f(0.5, 1.0, 0.3, 1.0);
            glTranslatef(pos[0] - XWidth / 2., pos[1] -YWidth / 2., pos[2] - ZWidth / 2.);
            glutSolidSphere(sizeSphereSurface, 50, 50);
            glTranslatef(-pos[0] + XWidth / 2., -pos[1] +YWidth / 2., -pos[2] + ZWidth / 2.);
        }
    }

    for(unsigned int is = 0; is < currentPoints.size(); ++is){
        vector<double> pos = currentPoints[is];
        if(pos.size() != 3) {
            cerr << "Incorrect size of point position (currentPoints[" << is << "]\n";
            continue;
        }
        //stringstream tt;
        //tt << "S="<< s->sequence;
        //drawBitmapText(tt.str().c_str(),-XWidth / 3.0, -YWidth/2.0 -is*5 , 0);

        glColor4f(0.5, 1.0, 0.3, 1.0);
        glTranslatef(pos[0] , pos[1] , pos[2] );
        glutSolidSphere(sizeSphereSurface, 50, 50);
        glTranslatef(-pos[0] , -pos[1] , -pos[2] );
    }


    //This is to say display everything now
    //glFlush();        // in the single buffer mode
    glutSwapBuffers();  // in the double buffers mode (see init)

    // To export the image as a bmp
    /*
    int w = glutGet( GLUT_WINDOW_WIDTH );
    int h = glutGet( GLUT_WINDOW_HEIGHT );
    vector< unsigned char > buf( w * h * 3 );
    glPixelStorei( GL_PACK_ALIGNMENT, 1 );
    glReadPixels( 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, &buf[0] );

    static int cpt = 1000;
    cpt ++;
    stringstream newFile;
    newFile << "im" <<  cpt << ".bmp";
    int err = SOIL_save_image(newFile.str().c_str(), SOIL_SAVE_TYPE_BMP, w, h, 3, &buf[0]);
    */
}





/*int bytesToUsePerPixel = 24 ;  // 16 bit per channel
 *
ILuint currentImage = ilGenImage();
//ILuint ilTexName[2]; //DevIL images
//ilGenImages(2, ilTexName); //Generate DevIL image objects
//ilBindImage(ilTexName[0]);
ilBindImage(currentImage);


int sizeOfByte = sizeof( unsigned char ) ;
//int theSize = w * h * sizeOfByte * bytesToUsePerPixel ;
ilTexImage(w,h,1,bytesToUsePerPixel,GL_LUMINANCE,IL_UNSIGNED_BYTE,&buf[0]);
ilLoadL()

static int cpt = 0;
cpt ++;
QString z = QString("im") + QString::number(cpt);
ilEnable(IL_FILE_OVERWRITE);
ilSave(IL_PNG, z.toStdString().c_str());

//ILubyte *brickData, *globeData; //DevIL image data
//brickData = ilGetData(); //Get image data from DevIL

GLuint textureID;
glGenTextures(1, &textureID);
glBindTexture(GL_TEXTURE_2D, textureID);
glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE); // automatic mipmap
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 512, 512, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
glBindTexture(GL_TEXTURE_2D, 0);*/

// create a renderbuffer object to store depth info
/*GLuint rboId;
glGenRenderbuffersEXT(1, &amp;rboId);
glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, rboId);
glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, 512, 512);
glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

// create a framebuffer object
GLuint fboId;
glGenFramebuffersEXT(1, &amp;fboId);
glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fboId);

// attach the texture to FBO color attachment point
glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, tex, 0);

// attach the renderbuffer to depth attachment point
glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, rboId);

//this is where I draw all of my scene with the exception of the sphere. too much code right here to post

// unbind FBO
glDeleteRenderbuffersEXT(1, &amp;rboId);
glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
}
*/






/*
GLfloat body[][2] = { {0, 3}, {1, 1}, {5, 1}, {8, 4}, {10, 4}, {11, 5},
  {11, 11.5}, {13, 12}, {13, 13}, {10, 13.5}, {13, 14}, {13, 15}, {11, 16},
  {8, 16}, {7, 15}, {7, 13}, {8, 12}, {7, 11}, {6, 6}, {4, 3}, {3, 2},
  {1, 2} };
GLfloat arm[][2] = { {8, 10}, {9, 9}, {10, 9}, {13, 8}, {14, 9}, {16, 9},
  {15, 9.5}, {16, 10}, {15, 10}, {15.5, 11}, {14.5, 10}, {14, 11}, {14, 10},
  {13, 9}, {11, 11}, {9, 11} };
GLfloat leg[][2] = { {8, 6}, {8, 4}, {9, 3}, {9, 2}, {8, 1}, {8, 0.5}, {9, 0},
  {12, 0}, {10, 1}, {10, 2}, {12, 4}, {11, 6}, {10, 7}, {9, 7} };
GLfloat eye[][2] = { {8.75, 15}, {9, 14.7}, {9.6, 14.7}, {10.1, 15},
  {9.6, 15.25}, {9, 15.25} };
GLfloat lightZeroPosition[] = {10.0, 4.0, 10.0, 1.0};
GLfloat lightZeroColor[] = {0.8, 1.0, 0.8, 1.0}; // green-tinted
GLfloat lightOnePosition[] = {-1.0, -2.0, 1.0, 0.0};
GLfloat lightOneColor[] = {0.6, 0.3, 0.2, 1.0}; // red-tinted
GLfloat skinColor[] = {0.1, 1.0, 0.1, 1.0}, eyeColor[] = {1.0, 0.2, 0.2, 1.0};



void
extrudeSolidFromPolygon(GLfloat data[][2], unsigned int dataSize,
  GLdouble thickness, GLuint side, GLuint edge, GLuint whole)
{
  static GLUtriangulatorObj *tobj = NULL;
  GLdouble vertex[3], dx, dy, len;
  int i;
  int count = (int) (dataSize / (2 * sizeof(GLfloat)));

  if (tobj == NULL) {
    tobj = gluNewTess();  // create and initialize a GLU polygon tesselation object
    gluTessCallback(tobj, GLU_BEGIN, glBegin);
    gluTessCallback(tobj, GLU_VERTEX, glVertex2fv);  // semi-tricky
    gluTessCallback(tobj, GLU_END, glEnd);

  }
  glNewList(side, GL_COMPILE);
  glShadeModel(GL_SMOOTH);  // smooth minimizes seeing tessellation
  gluBeginPolygon(tobj);
  for (i = 0; i < count; i++) {
    vertex[0] = data[i][0];
    vertex[1] = data[i][1];
    vertex[2] = 0;
    gluTessVertex(tobj, vertex, data[i]);
  }
  gluEndPolygon(tobj);
  glEndList();
  glNewList(edge, GL_COMPILE);
  glShadeModel(GL_FLAT);  // flat shade keeps angular hands from being * * "smoothed"
  glBegin(GL_QUAD_STRIP);
  for (i = 0; i <= count; i++) {
    // mod function handles closing the edge
    glVertex3f(data[i % count][0], data[i % count][1], 0.0);
    glVertex3f(data[i % count][0], data[i % count][1], thickness);
    // Calculate a unit normal by dividing by Euclidean
       distance. We * could be lazy and use
       glEnable(GL_NORMALIZE) so we could pass in * arbitrary
       normals for a very slight performance hit.
    dx = data[(i + 1) % count][1] - data[i % count][1];
    dy = data[i % count][0] - data[(i + 1) % count][0];
    len = sqrt(dx * dx + dy * dy);
    glNormal3f(dx / len, dy / len, 0.0);
  }
  glEnd();
  glEndList();
  glNewList(whole, GL_COMPILE);
  glFrontFace(GL_CW);
  glCallList(edge);
  glNormal3f(0.0, 0.0, -1.0);  // constant normal for side
  glCallList(side);
  glPushMatrix();
  glTranslatef(0.0, 0.0, thickness);
  glFrontFace(GL_CCW);
  glNormal3f(0.0, 0.0, 1.0);  // opposite normal for other side
  glCallList(side);
  glPopMatrix();
  glEndList();
}*/


/*
void
makeDinosaur(void)
{
  extrudeSolidFromPolygon(body, sizeof(body), bodyWidth,
    BODY_SIDE, BODY_EDGE, BODY_WHOLE);
  extrudeSolidFromPolygon(arm, sizeof(arm), bodyWidth / 4,
    ARM_SIDE, ARM_EDGE, ARM_WHOLE);
  extrudeSolidFromPolygon(leg, sizeof(leg), bodyWidth / 2,
    LEG_SIDE, LEG_EDGE, LEG_WHOLE);
  extrudeSolidFromPolygon(eye, sizeof(eye), bodyWidth + 0.2,
    EYE_SIDE, EYE_EDGE, EYE_WHOLE);
  glNewList(DINOSAUR, GL_COMPILE);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, skinColor);
  glCallList(BODY_WHOLE);

  glPushMatrix();
  glTranslatef(0.0, 0.0, bodyWidth);
  glCallList(ARM_WHOLE);
  glCallList(LEG_WHOLE);
  glTranslatef(0.0, 0.0, -bodyWidth - bodyWidth / 4);
  glCallList(ARM_WHOLE);
  glTranslatef(0.0, 0.0, -bodyWidth / 4);
  glCallList(LEG_WHOLE);
  glTranslatef(0.0, 0.0, bodyWidth / 2 - 0.1);
  glMaterialfv(GL_FRONT, GL_DIFFUSE, eyeColor);
  glCallList(EYE_WHOLE);
  glPopMatrix();
  glEndList();
}*/

#endif
