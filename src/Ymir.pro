SOURCES += \
    compact.cpp \
    lattice.cpp \
    proteins.cpp \
    receptorligand.cpp \
    fastaffinity.cpp \
    plot3d.cpp \
    stopwatch.cpp \
    zaptrackball.cpp \
    main.cpp \
    zaprandom.cpp \
    distribution.cpp \
    md5.cpp

HEADERS += \
    compact.h \
    lattice.h \
    proteins.h \
    receptorligand.h \
    fastaffinity.h \
    plot3d.h \
    stopwatch.h \
    ymir.h \
    zaptrackball.h \
    zaprandom.h \
    distribution.h \
    md5.h


QMAKE_CXXFLAGS += -std=c++11 -Wno-unused-parameter

unix: LIBS += -lglut -lGLU -lGL -lSOIL

win32: LIBS += -L$$PWD/freeglut/lib/ -lfreeglut -lopengl32
INCLUDEPATH += $$PWD/freeglut/include
DEPENDPATH += $$PWD/freeglut/lib

win32: LIBS += -L$$PWD/soil/ -lSOIL
INCLUDEPATH += $$PWD/soil
DEPENDPATH += $$PWD/soil

win32: LIBS += -L$$PWD/glStatic/ -lglu32
win32: LIBS += -L$$PWD/glStatic/ -lopengl32
INCLUDEPATH += $$PWD/glStatic
DEPENDPATH += $$PWD/glStatic

win32: TARGET=Ymir.exe
unix: TARGET = Ymir

